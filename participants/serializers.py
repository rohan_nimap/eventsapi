from rest_framework import serializers
from . import models

class EventSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = models.Event


class ParticipantSerializer(serializers.ModelSerializer):
    event = EventSerializer()

    class Meta:
        fields = ['id','name','email','mobile','event']
        model = models.Participant

class ParticipantCreateSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['name','email','mobile','event']
        model = models.Participant
    
class EventDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['id','event','date','name']
        model = models.EventSchedule