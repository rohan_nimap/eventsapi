# Create your models here.
# posts/models.py
from django.db import models


class Event(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta :
        # Explicitly define the name of the table
        db_table = "event"

class Participant(models.Model):
    name = models.CharField(max_length=50)
    email = models.TextField()
    mobile = models.IntegerField()
    event = models.ForeignKey(Event,on_delete=models.CASCADE,default=1)

    def __str__(self):
        return self.name

    class Meta : 
        # Explicitly define the name of the products
        db_table = 'participant'

class EventSchedule(models.Model):
    event = models.ForeignKey(Event,on_delete=models.CASCADE,default=1)
    date = models.DateField()
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
    
    class Meta:
        db_table = 'event_schedule'
