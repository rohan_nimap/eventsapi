from django.shortcuts import render
from django.http import request
# Create your views here.
from rest_framework import generics
from .models import Participant,Event,EventSchedule
from .serializers import ParticipantSerializer,ParticipantCreateSerializer,EventSerializer,EventDetailsSerializer


class ParticipantList(generics.ListAPIView):
    # /api/v1/participants/
    # Shows the list of participants
    serializer_class = ParticipantSerializer
    def get_queryset(self):

        queryset = Participant.objects.all()
        event = self.request.query_params.get('event', None)
        if event is not None:
            queryset = queryset.filter(event_id=event)
        return queryset

class ParticipantCreateAPIView(generics.CreateAPIView):
    # POST REQ -> participants/register/ -> Creates Participants 
    serializer_class = ParticipantCreateSerializer
    queryset = Participant.objects.all()

class ParticipantDetail(generics.RetrieveUpdateDestroyAPIView):
    # GET,PUT,PATCH
    # participants/1/ -> Give Detailed INdividual info
    queryset = Participant.objects.all()
    serializer_class = ParticipantCreateSerializer

class EventList(generics.ListAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

class EventDetails(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = EventDetailsSerializer
    def get_queryset(self):
        queryset = EventSchedule.objects.all()
        eid = self.request.query_params.get('eid',None)
        if eid is not None:
            queryset = queryset.filter(event_id=eid)
        return queryset   


    