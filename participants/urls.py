from django.urls import path

from . import views

urlpatterns = [
    # Lists
    path('participants/', views.ParticipantList.as_view()),
    path('events/', views.EventList.as_view()),

    #Detailed Info
    path('participants/<int:pk>/', views.ParticipantDetail.as_view()),
    path('events/<int:pk>/',views.EventDetails.as_view()),

    # Creates 
    path('participants/register/',views.ParticipantCreateAPIView.as_view()),
]